// Express setup
const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes.js');

const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/tasks', taskRoutes);

// Mongoose Connection
mongoose.connect(`mongodb+srv://juanpablocodes:admin123@clusterbatch-197.xubg08v.mongodb.net/s36?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));

app.listen(port, () => {
	console.log(`Server is running at port ${port}`);
});
