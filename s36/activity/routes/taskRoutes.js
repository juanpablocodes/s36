const express = require('express');

const TaskController = require('../controllers/TaskController.js');

const router = express.Router();

router.get('/', (req, res) => {
	TaskController.getAllTasks().then((resultFromController) => {
		res.send(resultFromController)
	})
});

router.get('/:id', (req, res) => {
	TaskController.getTask(req.params.id).then((resultFromController) => {
		res.send(resultFromController)
	})
});

router.post('/create', (req, res) => {
	TaskController.createTask(req.body).then((resultFromController) => {
		res.send(resultFromController)
	})
});

router.put('/:id', (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then((resultFromController) => {
			res.send(resultFromController)
	})
});

router.delete('/:id/delete', (req, res) => {
	TaskController.deleteTask(req.params.id).then((resultFromController) => {
		res.send(resultFromController)
	})
});

module.exports = router;
